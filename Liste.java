class Liste{
    private int val;
    private Liste suiv;
    
    //attention : utilisez plutôt "getVal()" que "val", car getVal() renvoie une exception quand la liste est vide,
	//et vous empêche donc d'utiliser la valeur "fantôme" du dernier maillon

    //liste vide =_def (*,null)
 
    //////////////////////////////////////////////
    //////// méthodes fournies
    //////////////////////////////////////////////
    
    public Liste(){
	suiv = null;
    }

    public Liste(Liste l){
	if(l.estVide()){
	    suiv=null;
	}
	else{
	    val = l.val;
	    suiv = new Liste(l.suiv);
	}
    }

    public Liste(int x, Liste l){
	val = x;
	suiv = new Liste(l);
    }

	public int getVal(){
		//sur liste non vide
		if(estVide())
			throw new RuntimeException("getVal appelée sur liste vide");
		return val;
	}
	public Liste getSuiv(){
		return suiv;
	}

	public void ajoutTete(int x){
		if(estVide()){
			val = x;
			suiv = new Liste();
		}
		else {
			Liste aux = new Liste();
			aux.val = getVal();
			aux.suiv = suiv;
			val = x;
			suiv = aux;
		}
	}

    public void supprimeTete(){
		//sur liste non vide
		if(suiv.estVide()){
			suiv = null;
		}
		else {
			val = suiv.getVal();
			suiv = suiv.suiv;
		}
    }

    public boolean estVide(){
	return suiv==null;
    }



    public String toString(){
	if(estVide()){
	    return "()";
	}
	else{
	    return getVal()+" "+suiv.toString();
	}
    }



    //////////////////////////////////////////////
    //////// méthodes du TD
    //////////////////////////////////////////////

	public int longueur(){
		if (suiv.estVide()) return 1;
		return 1+suiv.longueur();
	}

	public int somme() {
		if (estVide()) {
			return 0;
		} else {
			return getVal() + getSuiv().somme();
		}
	}

	public boolean croissant() {
		if (estVide() || getSuiv().estVide()) {
			return true; // An empty list or a single element list is always considered sorted.
		} else {
			return getVal() <= getSuiv().getVal() && getSuiv().croissant();
		}
	}

	public int get(int i) {
		if (i < 0 || i >= longueur()) {
			throw new IndexOutOfBoundsException("Index out of bounds");
		}

		Liste current = this;
		for (int j = 0; j < i; j++) {
			current = current.getSuiv();
		}

		return current.getVal();
	}

	public void ajoutFin(int x){
		if (suiv.estVide()){
			this.suiv.suiv= new Liste();
			this.suiv.val = x;
		}else{
			suiv.ajoutFin(x);
		}
	}

	public void concat(Liste l){
		if (suiv.estVide()) suiv = l;
		else suiv.concat(l);
	}

	public Liste supprOccs(int x){
		Liste l = new Liste();
		if (this.estVide()) return l;
		if(val==x){
			l = suiv.supprOccs(x);
		}else{
			l = new Liste(this);
			l.suiv = suiv.supprOccs(x);
		}
		return l;
	}

	public Liste supprOccsV2(int x) {
		if (estVide()) {
			return this;
		}
		if (val == x) {
			suiv = suiv.supprOccsV2(x);
			return suiv;
		}
		suiv = suiv.supprOccsV2(x);
		return this;
	}

	public Liste retourne(){

	}

	public static void main(String[] arg){
		Liste l = new Liste();
		l.ajoutTete(4);
		l.ajoutTete(3);
		l.ajoutTete(2);
		System.out.println("l : " + l);
		System.out.println(l.somme());
		System.out.println(l.croissant());
		System.out.println(l.get(2));
		l.supprimeTete();
		System.out.println("l : " + l);
		l.supprimeTete();
		System.out.println("l : " + l);
		System.out.println(l.longueur());
		l.supprimeTete();
		System.out.println("l : " + l);
		Liste l1 = new Liste();
		l1.ajoutTete(1);
		System.out.println(l1);
		Liste l2 = new Liste();
		l2.ajoutTete(2);
		l2.ajoutFin(3);
		System.out.println(l2);
		l1.concat(l2);
		System.out.println(l1);
		l2.val=52;
		System.out.println(l1);
		l2.suiv.val=31;
		System.out.println(l1);
		System.out.println(l1.supprOccs(52));
		System.out.println(l1);
		System.out.println(l1.supprOccsV2(52));
		System.out.println(l1);

	}
}
